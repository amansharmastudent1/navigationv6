import React from 'react';
import {Button, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Login = ({navigation}) => {
  return (
    <>
      <Text>I m Login</Text>
      <TouchableOpacity>
        <Button
          title="Back to Home"
          onPress={() => navigation.navigate('Home')}
        />
      </TouchableOpacity>
    </>
  );
};

export default Login;
