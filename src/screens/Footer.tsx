import React from 'react';
import {Button, Text, TouchableOpacity} from 'react-native';

const Footer = ({navigation}) => {
  return (
    <>
      <Text>I m Footer</Text>
      <Text>Navigate on Footer</Text>
      <TouchableOpacity>
        <Button title="Back to Home" onPress={() => navigation.navigate('Home')} />
      </TouchableOpacity>
    </>
  );
};

export default Footer;
