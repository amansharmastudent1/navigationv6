import React from 'react';
import {Button, Text, TouchableOpacity} from 'react-native';

const Home = ({navigation}) => {
  return (
    <>
      <Text>I m Home</Text>
      <Text>Navigate on Home</Text>
      <TouchableOpacity>
        <Button
          title="Move on Login"
          onPress={() => navigation.navigate('Login')}
        />
        <Button
          title="Move on Footer"
          onPress={() => navigation.navigate('Footer')}
        />
      </TouchableOpacity>
    </>
  );
};

export default Home;
