import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {authStack} from './Routes';

export const Navigator = () => {
  return <NavigationContainer>{authStack()}</NavigationContainer>;
};
