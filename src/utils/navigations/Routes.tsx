import * as React from 'react';
import Login from '../../screens/Login';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../../screens/Home';
import Footer from '../../screens/Footer';
const Stack = createNativeStackNavigator();

export enum ScreenNames {
  LOGIN = 'Login',
  HOME = 'Home',
  Footer = 'Footer',
}

export const authStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenNames.HOME}>
      <Stack.Screen name={ScreenNames.HOME} component={Home} />
      <Stack.Screen name={ScreenNames.LOGIN} component={Login} />
      <Stack.Screen name={ScreenNames.Footer} component={Footer} />
    </Stack.Navigator>
  );
};
